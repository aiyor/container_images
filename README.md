# Repository for Docker Build Files

This is a repository for central Dockerfiles.

Each Docker image build file(s) should reside in its own directory.


## Gitlab CI Pipeline

There is a CI template included in this repository to build Docker image using 
Kaniko (https://github.com/GoogleContainerTools/kaniko).

Kaniko was chosen as the tool to build container images as it can run the build
process **without** requiring privileged access or using DinD (which is not supported
by many SaaS CI support).

The Kaniko build process is created as a Gitlab CI template, in 
`ci-templates/build_docker.yml`, to make it easy to reference from any CI job.
The following variables should be defined in a CI job when using the template,

* `this_path` - The location of the Dockerfile of the job. As each container image build should reside in its own directory, this variable will let Kaniko know where to locate the Dockerfile.
* `image_name` - The name of the container image to be built.

### Referencing Gitlab CI Template

It is quite simple to reference the Kaniko build template in a CI job in Gitlab, 
simply include the template file using `include` directive in the CI job, e.g.,

```yaml
include:
  - local: /ci-templates/build_docker.yml
```

then in the `script` section, reference the template's script steps as below,

```yaml
script:
  - !reference [.build_docker, script]
```

Refer to example in `ansible/.gitlab-ci.yml` for more example/details.


### Tag Container Image with Version

When building container images, it is highly recommended to add `LABEL version` 
in `Dockerfile` to specify the image tag.  The pipeline template will look for
the `LABEL version` directive, and use the value of the version number as image
tag.
