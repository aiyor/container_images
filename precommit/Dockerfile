# pull base image
FROM python:alpine

ARG TF_BASE_URL="https://releases.hashicorp.com/terraform"
ARG TF_VERSION="1.5.5"
ARG TFDOCS_BASE_URL="https://github.com/terraform-docs/terraform-docs/releases/download"
ARG TFDOCS_VERSION="0.16.0"
ARG TFLINT_BASE_URL="https://github.com/terraform-linters/tflint/releases/download"
ARG TFLINT_VERSION="0.49.0"
ARG TFSEC_BASE_URL="https://github.com/aquasecurity/tfsec/releases/download"
ARG TFSEC_VERSION="1.28.4"

LABEL version="1.1.0"
LABEL org.opencontainers.image.authors="Tze Liang (gitlab.com/tze)"
LABEL description="This image contains required tools to run pre-commit hooks."


COPY apk-packages.txt pip-requirements.txt /tmp/

RUN echo "===> Base layer: Core utilities..." \
    && cat /tmp/apk-packages.txt | xargs apk add --no-cache \
    \
    \
    && echo "--> Installing using pip pip-requirements.txt..." \
    && pip install --no-cache-dir -r /tmp/pip-requirements.txt \
    \
    \
    && echo "--> Install Terraform..." \
    && cd /tmp \
    && curl -Lo ./terraform.zip ${TF_BASE_URL}/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip \
    && unzip terraform.zip \
    && mv terraform /usr/local/bin \
    && rm -f terraform.zip \
    && echo "--> Install terraform-docs..." \
    && cd /tmp \
    && curl -Lo ./terraform-docs.tar.gz ${TFDOCS_BASE_URL}/v${TFDOCS_VERSION}/terraform-docs-v${TFDOCS_VERSION}-linux-amd64.tar.gz \
    && tar -xzf terraform-docs.tar.gz \
    && chmod +x terraform-docs \
    && mv terraform-docs /usr/local/bin \
    && rm -f terraform-docs.tar.gz \
    \
    \
    && echo "--> Install tflint..." \
    && curl -Lo ./tflint.zip ${TFLINT_BASE_URL}/v${TFLINT_VERSION}/tflint_linux_amd64.zip \
    && unzip tflint.zip \
    && mv tflint /usr/local/bin \
    && rm -f tflint.zip \
    \
    \
    && echo "--> Install tfsec..." \
    && curl -Lo /usr/local/bin/tfsec ${TFSEC_BASE_URL}/v${TFSEC_VERSION}/tfsec-checkgen-linux-amd64 \
    && chmod +x /usr/local/bin/tfsec \
    \
    \
    && echo "--> Post processing..." \
    && sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd \
    && pip cache purge
